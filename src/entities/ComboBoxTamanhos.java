package entities;

public enum ComboBoxTamanhos {
	CM_0("0 cm"),
	CM_10("10 cm"),
	CM_15("15 cm"),
	CM_20("20 cm"),
	CM_25("25 cm"),
	CM_30("30 cm"),
	CM_35("35 cm"),
	CM_40("40 cm"),
	CM_45("45 cm"),
	CM_50("50 cm"),
	CM_55("55 cm"),
	CM_60("60 cm"),
	CM_65("65 cm"),
	CM_70("70 cm"),
	CM_75("75 cm"),
	CM_80("80 cm"),
	CM_85("85 cm"),
	CM_90("90 cm"),
	CM_95("95 cm"),
	CM_100("1,00 m"),
	CM_105("1,05 m"),
	CM_110("1,10 m"),
	CM_115("1,15 m"),
	CM_120("1,20 m"),
	CM_125("1,25 m"),
	CM_130("1,30 m"),
	CM_135("1,35 m"),
	CM_140("1,40 m"),
	CM_145("1,45 m"),
	CM_150("1,50 m"),
	CM_155("1,55 m"),
	CM_160("1,60 m"),
	CM_165("1,65 m"),
	CM_170("1,70 m"),
	CM_175("1,75 m"),
	CM_180("1,80 m"),
	CM_185("1,85 m"),
	CM_190("1,90 m"),
	CM_195("1,95 m"),
	CM_200("2,00 m"),
	CM_205("2,05 m"),
	CM_210("2,10 m"),
	CM_215("2,15 m"),
	CM_220("2,20 m"),
	CM_225("2,25 m"),
	CM_230("2,30 m"),
	CM_235("2,35 m"),
	CM_240("2,40 m"),
	CM_245("2,45 m"),
	CM_250("2,50 m"),
	CM_255("2,55 m"),
	CM_260("2,60 m"),
	CM_265("2,65 m"),
	CM_270("2,70 m"),
	CM_275("2,75 m"),
	CM_280("2,80 m"),
	CM_285("2,85 m"),
	CM_290("2,90 m"),
	CM_295("2,95 m"),
	CM_300("3,00 m");

	private String tamanho;

	private ComboBoxTamanhos(String tamanho) {
		this.tamanho = tamanho;
	}

	@Override
	public String toString() {
		return tamanho;
	}
}
