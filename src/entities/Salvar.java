package entities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

import javax.swing.JOptionPane;

import org.apache.commons.lang3.StringUtils;

public class Salvar {

	private static final Logger LOGGER = Logger.getLogger(Salvar.class.getName());

	private Salvar() {
		throw new IllegalStateException("Classe que gera os arquivos txt de or�amentos");
	}

	public static void salvarOrcamento(Orcamento orcamento) {

		Cliente cliente = new Cliente();
		boolean ok = false;

		while (!ok) {
			cliente.setNome(JOptionPane.showInputDialog(null, "Nome do cliente:"));
			if (StringUtils.isBlank(cliente.getNome())) {
				JOptionPane.showMessageDialog(null, "Necess�rio um nome!");
			} else {

				SimpleDateFormat date = new SimpleDateFormat("yyyy-MM-dd");
				File jarDir = new File(ClassLoader.getSystemClassLoader().getResource(".").getPath());
				String folder = jarDir.toString() + "\\Or�amentos";
				String subFolder = folder + "\\" + date.format(new Date());
				new File(folder).mkdir();
				new File(subFolder).mkdir();
				try (OutputStreamWriter file = new OutputStreamWriter(
						new FileOutputStream(subFolder + "\\" + cliente.getNome() + ".txt", true), "UTF-8")) {
					file.write("==============================\n");
					file.write("> " + cliente.getNome() + "\n");
					file.write("> R$ " + String.format("%.2f", orcamento.getTotal()) + "\n");
					file.write("==============================\n");
					ok = true;
				} catch (IOException e1) {
					JOptionPane.showMessageDialog(null, "N�o foi poss�vel gravar em arquivo", "Erro de escrita",
							JOptionPane.ERROR_MESSAGE, null);
					LOGGER.log(null, "N�o foi poss�vel gravar no arquivo TXT", e1);
				}
			}
		}
	}
}