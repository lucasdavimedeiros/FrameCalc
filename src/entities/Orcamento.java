package entities;

public class Orcamento {

	private double tamanhoInterno1;
	private double tamanhoInterno2;
	private double tamanhoExterno1;
	private double tamanhoExterno2;
	private double moldura1;
	private double moldura2;
	private double moldura3;
	private double moldura4;
	private double vidro;
	private double vidroTraseiro;
	private double maoDeObra;
	private double eucatex;
	private double paspatur;
	private int sarrafo;
	private double espelho;
	public static final int VIDRO_COMUM = 40;
	public static final int VIDRO_ANTIRREFLEXO = 80;
	public static final int SEM_VIDRO = 0;
	public static final int MAO_DE_OBRA = 12;
	public static final int EUCATEX_VALOR = 15;
	public static final int PASPATUR_VALOR = 50;
	public static final int SARRAFO_VALOR = 10;
	public static final int ESPELHO_VALOR = 120;
	private double total;

	public double getTamanhoInterno1() {
		return tamanhoInterno1;
	}

	public void setTamanhoInterno1(double tamanhoInterno1) {
		this.tamanhoInterno1 = tamanhoInterno1;
	}

	public double getTamanhoInterno2() {
		return tamanhoInterno2;
	}

	public void setTamanhoInterno2(double tamanhoInterno2) {
		this.tamanhoInterno2 = tamanhoInterno2;
	}

	public double getTamanhoExterno1() {
		return tamanhoExterno1;
	}

	public void setTamanhoExterno1(double tamanhoExterno1) {
		this.tamanhoExterno1 = tamanhoExterno1;
	}

	public double getTamanhoExterno2() {
		return tamanhoExterno2;
	}

	public void setTamanhoExterno2(double tamanhoExterno2) {
		this.tamanhoExterno2 = tamanhoExterno2;
	}

	public double getTamanhoExterno() {
		double externo = (getTamanhoExterno1() * 2) + (getTamanhoExterno2() * 2);
		if (externo < 1) {
			externo = 1;
		}
		return externo;
	}

	public double getAreaInterno() {
		return getTamanhoInterno1() * getTamanhoInterno2();
	}

	public double getMoldura1() {
		return moldura1;
	}

	public void setMoldura1(int moldura1) {
		double calc = Math.ceil(getTamanhoExterno() * moldura1);
		this.moldura1 = calc;
	}

	public double getMoldura2() {
		return moldura2;
	}

	public void setMoldura2(int moldura2) {
		double calc = Math.ceil(getTamanhoExterno() * moldura2);
		this.moldura2 = calc;
	}

	public double getMoldura3() {
		return moldura3;
	}

	public void setMoldura3(int moldura3) {
		double calc = Math.ceil(getTamanhoExterno() * moldura3);
		this.moldura3 = calc;
	}

	public double getMoldura4() {
		return moldura4;
	}

	public void setMoldura4(int moldura4) {
		double calc = Math.ceil(getTamanhoExterno() * moldura4);
		this.moldura4 = calc;
	}

	public double getVidro() {
		return vidro;
	}

	public void setVidro(int vidro) {
		double calc = Math.ceil(getAreaInterno() * vidro);
		this.vidro = calc;
	}

	public double getVidroTraseiro() {
		return vidroTraseiro;
	}

	public void setVidroTraseiroSV() {
		double calc = Math.ceil(getAreaInterno() * VIDRO_COMUM);
		vidroTraseiro = calc;
	}

	public double getMaoDeObra() {
		return maoDeObra;
	}

	public void setMaoDeObra(double maoDeObra) {
		this.maoDeObra = Math.ceil(maoDeObra);
	}

	public double getEucatex() {
		return eucatex;
	}

	public void setEucatex(double eucatex) {
		this.eucatex = Math.ceil(eucatex);
	}

	public double getPaspatur() {
		return paspatur;
	}

	public void setPaspatur(double paspatur) {
		this.paspatur = Math.ceil(paspatur);
	}

	public int getSarrafo() {
		return sarrafo;
	}

	public void setSarrafo(int sarrafo) {
		this.sarrafo = sarrafo;
	}

	public double getEspelho() {
		return espelho;
	}

	public void setEspelho(double espelho) {
		this.espelho = Math.ceil(espelho);
	}

	public double getTotal() {
		return total;
	}

	public String getTotalToString() {
		return String.valueOf(total);
	}

	public void setTotal(double total) {
		this.total = total;
	}
}
