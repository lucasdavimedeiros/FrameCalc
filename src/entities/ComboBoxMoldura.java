package entities;

public enum ComboBoxMoldura {

	VALOR_0("0,00"),
	VALOR_10("10,00"),
	VALOR_12("12,00"),
	VALOR_15("15,00"),
	VALOR_18("18,00"),
	VALOR_20("20,00"),
	VALOR_25("25,00"),
	VALOR_30("30,00"),
	VALOR_35("35,00"),
	VALOR_40("40,00"),
	VALOR_45("45,00"),
	VALOR_50("50,00"),
	VALOR_55("55,00"),
	VALOR_60("60,00"),
	VALOR_65("65,00"),
	VALOR_70("70,00"),
	VALOR_75("75,00"),
	VALOR_80("80,00"),
	VALOR_85("85,00"),
	VALOR_90("90,00"),
	VALOR_95("95,00"),
	VALOR_100("100,00");

	private String moldura;

	private ComboBoxMoldura(String moldura) {
		this.moldura = moldura;
	}

	@Override
	public String toString() {
		return moldura;
	}
}
