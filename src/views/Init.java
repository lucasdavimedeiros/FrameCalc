package views;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.KeyEvent;
import java.util.Locale;

import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTabbedPane;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

import entities.ComboBoxMoldura;
import entities.ComboBoxTamanhos;
import entities.Orcamento;
import entities.Salvar;

public class Init extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8690484694777459889L;
	public static final String TIMES_NEW_ROMAN_FONT = "Times New Roman";
	private JPanel contentPane;
	private ButtonGroup buttonGroup = new ButtonGroup();
	private ButtonGroup buttonGroupSV = new ButtonGroup();
	private JTextField txtValor;
	private JTextField txtValorSV;
	JComboBox<ComboBoxTamanhos> comboBoxTamanhoInterno1 = new JComboBox<>();
	JComboBox<ComboBoxTamanhos> comboBoxTamanhoInterno2 = new JComboBox<>();
	JComboBox<ComboBoxTamanhos> comboBoxTamanhoExterno1 = new JComboBox<>();
	JComboBox<ComboBoxTamanhos> comboBoxTamanhoExterno2 = new JComboBox<>();
	JComboBox<ComboBoxTamanhos> comboBoxTamanhoInterno1SV = new JComboBox<>();
	JComboBox<ComboBoxTamanhos> comboBoxTamanhoInterno2SV = new JComboBox<>();
	JComboBox<ComboBoxTamanhos> comboBoxTamanhoExterno1SV = new JComboBox<>();
	JComboBox<ComboBoxTamanhos> comboBoxTamanhoExterno2SV = new JComboBox<>();
	JButton btnCalcular = new JButton("Calcular");
	JButton btnCalcularSV = new JButton("Calcular");
	JButton btnDetalhes = new JButton("Detalhes");
	JButton btnDetalhesSV = new JButton("Detalhes");
	JButton btnReplicar = new JButton("Replicar");
	JButton btnReplicarSV = new JButton("Replicar");
	JButton btnAddMoldura1 = new JButton("+");
	JButton btnAddMoldura2 = new JButton("+");
	JButton btnAddMoldura3 = new JButton("+");
	JButton btnAddMoldura4 = new JButton("+");
	JLabel lblPrecoMinimo = new JLabel("Pre\u00E7o m\u00EDnimo R$ 15,00");
	JComboBox<ComboBoxMoldura> comboBoxMoldura1 = new JComboBox<>();
	JComboBox<ComboBoxMoldura> comboBoxMoldura2 = new JComboBox<>();
	JComboBox<ComboBoxMoldura> comboBoxMoldura3 = new JComboBox<>();
	JComboBox<ComboBoxMoldura> comboBoxMoldura4 = new JComboBox<>();
	JSpinner spinNQuadros = new JSpinner();
	JSpinner spinNQuadrosSV = new JSpinner();
	JRadioButton rdbtnVidroComum = new JRadioButton("Comum");
	JRadioButton rdbtnVidroAntirreflexo = new JRadioButton("Antirreflexo");
	JRadioButton rdbtnVidroSem = new JRadioButton("Sem");
	JRadioButton rdbtnVidroComumSV = new JRadioButton("Comum");
	JRadioButton rdbtnVidroAntirreflexoSV = new JRadioButton("Antirreflexo");
	JCheckBox chckbxEucatex = new JCheckBox("Eucatex");
	JCheckBox chckbxPaspatur = new JCheckBox("Paspatur");
	JCheckBox chckbxMaoDeObra = new JCheckBox("M\u00E3o de obra");
	JCheckBox chckbxSarrafo = new JCheckBox("Sarrafo");
	JCheckBox chckbxEspelho = new JCheckBox("Espelho");
	JMenuItem menuArquivoSalvar;
	JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	JComboBox<ComboBoxMoldura> comboBoxMoldura1SV = new JComboBox<>();
	JComboBox<ComboBoxMoldura> comboBoxMoldura2SV = new JComboBox<>();
	JComboBox<ComboBoxMoldura> comboBoxMoldura3SV = new JComboBox<>();
	JComboBox<ComboBoxMoldura> comboBoxMoldura4SV = new JComboBox<>();
	JButton btnAddMoldura1SV = new JButton("+");
	JButton btnAddMoldura2SV = new JButton("+");
	JButton btnAddMoldura3SV = new JButton("+");
	JButton btnAddMoldura4SV = new JButton("+");
	JCheckBox chckbxMaoDeObraSV = new JCheckBox("M\u00E3o de obra");

	Orcamento orcamento = new Orcamento();
	Orcamento orcamentoSV = new Orcamento();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Init frame = new Init();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Init() {
		Locale.setDefault(new Locale("pt", "BR"));
		Image icon = Toolkit.getDefaultToolkit().getImage(this.getClass().getResource("/images/icon.png"));
		this.setIconImage(icon);
		setTitle("Or\u00E7amento - S\u00F3 Posters");
		setResizable(false);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 650, 470);
		setLocationRelativeTo(null);
		btnCalcular.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnCalcularSV.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnDetalhes.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnDetalhesSV.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnReplicar.setCursor(new Cursor(Cursor.HAND_CURSOR));
		btnReplicarSV.setCursor(new Cursor(Cursor.HAND_CURSOR));

		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);

		JMenu menuArquivo = new JMenu("Arquivo");
		menuArquivo.setMnemonic('A');
		menuBar.add(menuArquivo);

		JMenuItem menuArquivoNovo = new JMenuItem("Novo");
		menuArquivoNovo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (tabbedPane.getSelectedIndex() == 0) {
					comboBoxTamanhoInterno1.setSelectedIndex(0);
					comboBoxTamanhoInterno2.setSelectedIndex(0);
					comboBoxTamanhoExterno1.setSelectedIndex(0);
					comboBoxTamanhoExterno2.setSelectedIndex(0);
					comboBoxMoldura1.setSelectedIndex(0);
					comboBoxMoldura1.setEnabled(false);
					btnAddMoldura1.setText("+");
					comboBoxMoldura2.setSelectedIndex(0);
					comboBoxMoldura2.setEnabled(false);
					btnAddMoldura2.setText("+");
					comboBoxMoldura3.setSelectedIndex(0);
					comboBoxMoldura3.setEnabled(false);
					btnAddMoldura3.setText("+");
					comboBoxMoldura4.setSelectedIndex(0);
					comboBoxMoldura4.setEnabled(false);
					btnAddMoldura4.setText("+");
					rdbtnVidroSem.setSelected(true);
					chckbxMaoDeObra.setSelected(true);
					chckbxEucatex.setSelected(false);
					chckbxPaspatur.setSelected(false);
					chckbxSarrafo.setSelected(false);
					chckbxEspelho.setSelected(false);
					spinNQuadros.setEnabled(false);
					lblPrecoMinimo.setVisible(false);
					txtValor.setText("0,00");
					menuArquivoSalvar.setEnabled(false);
				} else {
					comboBoxTamanhoInterno1SV.setSelectedIndex(0);
					comboBoxTamanhoInterno2SV.setSelectedIndex(0);
					comboBoxTamanhoExterno1SV.setSelectedIndex(0);
					comboBoxTamanhoExterno2SV.setSelectedIndex(0);
					comboBoxMoldura1SV.setSelectedIndex(0);
					comboBoxMoldura1SV.setEnabled(false);
					btnAddMoldura1SV.setText("+");
					comboBoxMoldura2SV.setSelectedIndex(0);
					comboBoxMoldura2SV.setEnabled(false);
					btnAddMoldura2SV.setText("+");
					comboBoxMoldura3SV.setSelectedIndex(0);
					comboBoxMoldura3SV.setEnabled(false);
					btnAddMoldura3SV.setText("+");
					comboBoxMoldura4SV.setSelectedIndex(0);
					comboBoxMoldura4SV.setEnabled(false);
					btnAddMoldura4SV.setText("+");
					buttonGroupSV.clearSelection();
					chckbxMaoDeObraSV.setSelected(true);
					spinNQuadrosSV.setEnabled(false);
					txtValorSV.setText("0,00");
					menuArquivoSalvar.setEnabled(false);
				}
			}
		});
		menuArquivoNovo.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));

		JMenuItem menuArquivoSair = new JMenuItem("Sair");
		menuArquivoSair.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F4, InputEvent.ALT_MASK));
		menuArquivoSair.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(EXIT_ON_CLOSE);
			}
		});

		menuArquivoSalvar = new JMenuItem("Salvar");
		menuArquivoSalvar.setEnabled(false);
		menuArquivoSalvar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {

				Salvar.salvarOrcamento(orcamento);

			}
		});
		menuArquivoSalvar.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
		menuArquivo.add(menuArquivoNovo);
		menuArquivo.add(menuArquivoSalvar);
		menuArquivo.add(menuArquivoSair);

		JMenu menuInserir = new JMenu("Inserir");
		menuInserir.setMnemonic('I');
		menuBar.add(menuInserir);

		JMenu menuInserirTamanhoPadrao = new JMenu("Tamanho padr\u00E3o");
		menuInserir.add(menuInserirTamanhoPadrao);

		JMenuItem menuInserirTamanhoPadrao60x90 = new JMenuItem("60 x 90");
		menuInserirTamanhoPadrao60x90
				.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6, InputEvent.CTRL_MASK | InputEvent.SHIFT_MASK));
		menuInserirTamanhoPadrao60x90.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				comboBoxTamanhoInterno1.setSelectedIndex(11);
				comboBoxTamanhoInterno2.setSelectedIndex(17);
				tabbedPane.setSelectedIndex(0);
			}
		});
		menuInserirTamanhoPadrao.add(menuInserirTamanhoPadrao60x90);

		JMenuItem menuInserirTamanhoPadraoA0 = new JMenuItem("A0");
		menuInserirTamanhoPadraoA0.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxTamanhoInterno1.setSelectedIndex(23);
				comboBoxTamanhoInterno2.setSelectedIndex(16);
			}
		});
		menuInserirTamanhoPadraoA0
				.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_0, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuInserirTamanhoPadrao.add(menuInserirTamanhoPadraoA0);

		JMenuItem menuInserirTamanhoPadraoA1 = new JMenuItem("A1");
		menuInserirTamanhoPadraoA1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxTamanhoInterno1.setSelectedIndex(16);
				comboBoxTamanhoInterno2.setSelectedIndex(11);
			}
		});
		menuInserirTamanhoPadraoA1
				.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_1, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuInserirTamanhoPadrao.add(menuInserirTamanhoPadraoA1);

		JMenuItem menuInserirTamanhoPadraoA2 = new JMenuItem("A2");
		menuInserirTamanhoPadraoA2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxTamanhoInterno1.setSelectedIndex(11);
				comboBoxTamanhoInterno2.setSelectedIndex(8);
			}
		});
		menuInserirTamanhoPadraoA2
				.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_2, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuInserirTamanhoPadrao.add(menuInserirTamanhoPadraoA2);

		JMenuItem menuInserirTamanhoPadraoA3 = new JMenuItem("A3");
		menuInserirTamanhoPadraoA3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxTamanhoInterno1.setSelectedIndex(8);
				comboBoxTamanhoInterno2.setSelectedIndex(5);
			}
		});
		menuInserirTamanhoPadraoA3
				.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_3, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuInserirTamanhoPadrao.add(menuInserirTamanhoPadraoA3);

		JMenuItem menuInserirTamanhoPadraoA4 = new JMenuItem("A4");
		menuInserirTamanhoPadraoA4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxTamanhoInterno1.setSelectedIndex(5);
				comboBoxTamanhoInterno2.setSelectedIndex(4);
			}
		});
		menuInserirTamanhoPadraoA4
				.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_4, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuInserirTamanhoPadrao.add(menuInserirTamanhoPadraoA4);

		JMenuItem menuInserirTamanhoPadraoA5 = new JMenuItem("A5");
		menuInserirTamanhoPadraoA5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxTamanhoInterno1.setSelectedIndex(4);
				comboBoxTamanhoInterno2.setSelectedIndex(2);
			}
		});
		menuInserirTamanhoPadraoA5
				.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_5, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuInserirTamanhoPadrao.add(menuInserirTamanhoPadraoA5);

		JMenuItem menuInserirTamanhoPadraoA6 = new JMenuItem("A6");
		menuInserirTamanhoPadraoA6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				comboBoxTamanhoInterno1.setSelectedIndex(2);
				comboBoxTamanhoInterno2.setSelectedIndex(1);
			}
		});
		menuInserirTamanhoPadraoA6
				.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_6, InputEvent.CTRL_MASK | InputEvent.ALT_MASK));
		menuInserirTamanhoPadrao.add(menuInserirTamanhoPadraoA6);

		JMenu menuAjuda = new JMenu("Ajuda");
		menuAjuda.setMnemonic('J');
		menuBar.add(menuAjuda);

		JMenuItem menuAjudaSobre = new JMenuItem("Sobre");
		menuAjudaSobre.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new Sobre().setVisible(true);
			}
		});
		menuAjudaSobre.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
		menuAjuda.add(menuAjudaSobre);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);

		tabbedPane.setBounds(0, 0, 644, 420);
		contentPane.add(tabbedPane);

		JPanel abaMoldura = new JPanel();
		tabbedPane.addTab("Moldura", null, abaMoldura, null);
		abaMoldura.setLayout(null);

		JLabel lblXTI = new JLabel("X");
		lblXTI.setBounds(112, 41, 21, 24);
		lblXTI.setHorizontalAlignment(SwingConstants.CENTER);
		lblXTI.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 16));
		abaMoldura.add(lblXTI);

		JLabel lblXTE = new JLabel("X");
		lblXTE.setBounds(112, 115, 21, 24);
		lblXTE.setHorizontalAlignment(SwingConstants.CENTER);
		lblXTE.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 16));
		abaMoldura.add(lblXTE);

		JLabel lblTamanhosInternos = new JLabel("Tamanhos internos");
		lblTamanhosInternos.setBounds(30, 11, 138, 24);
		lblTamanhosInternos.setHorizontalAlignment(SwingConstants.CENTER);
		lblTamanhosInternos.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(lblTamanhosInternos);

		JLabel lblTamanhosExternos = new JLabel("Tamanhos externos");
		lblTamanhosExternos.setBounds(30, 84, 146, 24);
		lblTamanhosExternos.setHorizontalAlignment(SwingConstants.CENTER);
		lblTamanhosExternos.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(lblTamanhosExternos);

		btnReplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnReplicar(comboBoxTamanhoExterno1, comboBoxTamanhoInterno1);
				btnReplicar(comboBoxTamanhoExterno2, comboBoxTamanhoInterno2);
			}
		});
		btnReplicar.setBounds(216, 79, 80, 24);
		btnReplicar.setToolTipText("Copia os valores das caixas de tamanhos internos para externos");
		btnReplicar.setEnabled(false);
		btnReplicar.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 14));
		abaMoldura.add(btnReplicar);

		JLabel lblVidro = new JLabel("Vidro");
		lblVidro.setBounds(30, 178, 46, 24);
		lblVidro.setHorizontalAlignment(SwingConstants.LEFT);
		lblVidro.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 20));
		abaMoldura.add(lblVidro);

		rdbtnVidroComum.setBounds(85, 180, 83, 23);
		rdbtnVidroComum.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(rdbtnVidroComum);

		rdbtnVidroAntirreflexo.setBounds(170, 180, 111, 23);
		rdbtnVidroAntirreflexo.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(rdbtnVidroAntirreflexo);

		rdbtnVidroSem.setBounds(283, 180, 57, 23);
		rdbtnVidroSem.setSelected(true);
		rdbtnVidroSem.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(rdbtnVidroSem);

		buttonGroup.add(rdbtnVidroComum);
		buttonGroup.add(rdbtnVidroAntirreflexo);
		buttonGroup.add(rdbtnVidroSem);

		chckbxEucatex.setBounds(149, 235, 83, 31);
		chckbxEucatex.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(chckbxEucatex);

		chckbxPaspatur.setBounds(234, 235, 89, 31);
		chckbxPaspatur.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(chckbxPaspatur);

		chckbxMaoDeObra.setBounds(30, 235, 117, 31);
		chckbxMaoDeObra.setSelected(true);
		chckbxMaoDeObra.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(chckbxMaoDeObra);

		chckbxSarrafo.setBounds(30, 272, 79, 31);
		chckbxSarrafo.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(chckbxSarrafo);

		chckbxEspelho.setBounds(149, 272, 83, 31);
		chckbxEspelho.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(chckbxEspelho);

		btnCalcular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				orcamento.setTamanhoInterno1(getComboBoxTamanho(comboBoxTamanhoInterno1));
				orcamento.setTamanhoInterno2(getComboBoxTamanho(comboBoxTamanhoInterno2));
				orcamento.setTamanhoExterno1(getComboBoxTamanho(comboBoxTamanhoExterno1));
				orcamento.setTamanhoExterno2(getComboBoxTamanho(comboBoxTamanhoExterno2));
				orcamento.setMoldura1(getComboBoxMoldura(comboBoxMoldura1));
				orcamento.setMoldura2(getComboBoxMoldura(comboBoxMoldura2));
				orcamento.setMoldura3(getComboBoxMoldura(comboBoxMoldura3));
				orcamento.setMoldura4(getComboBoxMoldura(comboBoxMoldura4));
				orcamento.setVidro(getTipoVidro());
				orcamento.setMaoDeObra(getMaoDeObra(orcamento));
				orcamento.setEucatex(getEucatex(orcamento));
				orcamento.setPaspatur(getPaspatur(orcamento));
				orcamento.setSarrafo(getSarrafo());
				orcamento.setEspelho(getEspelho(orcamento));
				orcamento.setTotal(somaTotal(orcamento));
				txtValor.setText(String.format("%.2f", orcamento.getTotal()));
				btnDetalhes.setEnabled(true);
				spinNQuadros.setEnabled(true);
				spinNQuadros.setValue(1);
				precoMinimo(orcamento.getTotal());
				menuArquivoSalvar.setEnabled(true);
			}
		});
		btnCalcular.setEnabled(false);
		btnCalcular.setBounds(30, 335, 135, 35);
		btnCalcular.setToolTipText("");
		btnCalcular.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 22));
		abaMoldura.add(btnCalcular);

		JLabel lblMoldura1 = new JLabel("Moldura 1 R$");
		lblMoldura1.setBounds(400, 43, 101, 22);
		lblMoldura1.setHorizontalAlignment(SwingConstants.CENTER);
		lblMoldura1.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(lblMoldura1);

		JLabel lblMoldura2 = new JLabel("Moldura 2 R$");
		lblMoldura2.setBounds(400, 85, 101, 22);
		lblMoldura2.setHorizontalAlignment(SwingConstants.CENTER);
		lblMoldura2.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(lblMoldura2);

		JLabel lblMoldura3 = new JLabel("Moldura 3 R$");
		lblMoldura3.setBounds(400, 127, 101, 22);
		lblMoldura3.setHorizontalAlignment(SwingConstants.CENTER);
		lblMoldura3.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(lblMoldura3);

		JLabel lblMoldura4 = new JLabel("Moldura 4 R$");
		lblMoldura4.setBounds(400, 169, 101, 22);
		lblMoldura4.setHorizontalAlignment(SwingConstants.CENTER);
		lblMoldura4.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(lblMoldura4);

		comboBoxTamanhoInterno1.setBounds(30, 40, 76, 28);
		comboBoxTamanhoInterno1.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		comboBoxTamanhoInterno1.setMaximumRowCount(15);
		comboBoxTamanhoInterno1.setModel(new DefaultComboBoxModel<>(ComboBoxTamanhos.values()));
		abaMoldura.add(comboBoxTamanhoInterno1);

		comboBoxTamanhoInterno2.setBounds(140, 40, 76, 28);
		comboBoxTamanhoInterno2.setModel(new DefaultComboBoxModel<>(ComboBoxTamanhos.values()));
		comboBoxTamanhoInterno2.setMaximumRowCount(15);
		comboBoxTamanhoInterno2.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(comboBoxTamanhoInterno2);

		comboBoxTamanhoExterno1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				comboBoxTamanhosExternosZero(comboBoxTamanhoInterno1, comboBoxTamanhoInterno2, comboBoxTamanhoExterno1,
						comboBoxTamanhoExterno2, btnCalcular, btnDetalhes);
			}
		});
		comboBoxTamanhoExterno1.setBounds(30, 113, 76, 28);
		comboBoxTamanhoExterno1.setModel(new DefaultComboBoxModel<>(ComboBoxTamanhos.values()));
		comboBoxTamanhoExterno1.setMaximumRowCount(15);
		comboBoxTamanhoExterno1.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(comboBoxTamanhoExterno1);

		comboBoxTamanhoExterno2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				comboBoxTamanhosExternosZero(comboBoxTamanhoInterno1, comboBoxTamanhoInterno2, comboBoxTamanhoExterno1,
						comboBoxTamanhoExterno2, btnCalcular, btnDetalhes);
			}
		});
		comboBoxTamanhoExterno2.setBounds(140, 113, 76, 28);
		comboBoxTamanhoExterno2.setModel(new DefaultComboBoxModel<>(ComboBoxTamanhos.values()));
		comboBoxTamanhoExterno2.setMaximumRowCount(15);
		comboBoxTamanhoExterno2.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(comboBoxTamanhoExterno2);
		comboBoxMoldura1.setEnabled(false);
		comboBoxMoldura1.setBounds(511, 41, 75, 28);
		comboBoxMoldura1.setModel(new DefaultComboBoxModel<>(ComboBoxMoldura.values()));
		comboBoxMoldura1.setMaximumRowCount(15);
		comboBoxMoldura1.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(comboBoxMoldura1);

		comboBoxMoldura2.setEnabled(false);
		comboBoxMoldura2.setBounds(511, 82, 75, 28);
		comboBoxMoldura2.setModel(new DefaultComboBoxModel<>(ComboBoxMoldura.values()));
		comboBoxMoldura2.setMaximumRowCount(15);
		comboBoxMoldura2.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(comboBoxMoldura2);

		comboBoxMoldura3.setEnabled(false);
		comboBoxMoldura3.setBounds(511, 124, 75, 28);
		comboBoxMoldura3.setModel(new DefaultComboBoxModel<>(ComboBoxMoldura.values()));
		comboBoxMoldura3.setMaximumRowCount(15);
		comboBoxMoldura3.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(comboBoxMoldura3);

		comboBoxMoldura4.setEnabled(false);
		comboBoxMoldura4.setBounds(511, 166, 75, 28);
		comboBoxMoldura4.setModel(new DefaultComboBoxModel<>(ComboBoxMoldura.values()));
		comboBoxMoldura4.setMaximumRowCount(15);
		comboBoxMoldura4.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(comboBoxMoldura4);

		JLabel lblTotal = new JLabel("Total: R$");
		lblTotal.setBounds(395, 339, 97, 28);
		lblTotal.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotal.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 22));
		abaMoldura.add(lblTotal);

		btnDetalhes.setBounds(170, 339, 108, 27);
		btnDetalhes.setEnabled(false);
		btnDetalhes.setToolTipText("Exibe os valores de cada material separadamente");
		btnDetalhes.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		abaMoldura.add(btnDetalhes);

		txtValor = new JTextField();
		txtValor.setBackground(Color.WHITE);
		txtValor.setEditable(false);
		txtValor.setText(ComboBoxMoldura.VALOR_0.toString());
		txtValor.setForeground(Color.RED);
		txtValor.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 26));
		txtValor.setHorizontalAlignment(SwingConstants.RIGHT);
		txtValor.setBounds(493, 335, 135, 35);
		abaMoldura.add(txtValor);
		txtValor.setColumns(10);

		JLabel txtNQuadros = new JLabel("N. de quadros");
		txtNQuadros.setHorizontalAlignment(SwingConstants.CENTER);
		txtNQuadros.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		txtNQuadros.setBounds(467, 300, 102, 22);
		abaMoldura.add(txtNQuadros);

		spinNQuadros.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				double total = orcamento.getTotal();
				double spinner = Double.parseDouble(spinNQuadros.getValue().toString());
				double add = total * spinner;
				txtValor.setText(String.format("%.2f", add));
				precoMinimo(add);
			}
		});
		spinNQuadros.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		spinNQuadros.setEnabled(false);
		spinNQuadros.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		spinNQuadros.setBounds(579, 298, 50, 25);
		abaMoldura.add(spinNQuadros);
		btnAddMoldura1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddOnOff(btnAddMoldura1, comboBoxMoldura1);
			}
		});
		btnAddMoldura1.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 16));
		btnAddMoldura1.setBounds(588, 42, 43, 26);
		abaMoldura.add(btnAddMoldura1);

		btnAddMoldura2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddOnOff(btnAddMoldura2, comboBoxMoldura2);
			}
		});
		btnAddMoldura2.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 16));
		btnAddMoldura2.setBounds(588, 83, 43, 26);
		abaMoldura.add(btnAddMoldura2);

		btnAddMoldura3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddOnOff(btnAddMoldura3, comboBoxMoldura3);
			}
		});
		btnAddMoldura3.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 16));
		btnAddMoldura3.setBounds(588, 125, 43, 26);
		abaMoldura.add(btnAddMoldura3);

		btnAddMoldura4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddOnOff(btnAddMoldura4, comboBoxMoldura4);
			}
		});

		btnAddMoldura4.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 16));
		btnAddMoldura4.setBounds(588, 167, 43, 26);
		abaMoldura.add(btnAddMoldura4);

		lblPrecoMinimo.setHorizontalAlignment(SwingConstants.CENTER);
		lblPrecoMinimo.setForeground(Color.BLUE);
		lblPrecoMinimo.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD | Font.ITALIC, 18));
		lblPrecoMinimo.setBounds(449, 267, 179, 22);
		abaMoldura.add(lblPrecoMinimo);
		lblPrecoMinimo.setVisible(false);

		JPanel abaSandVidro = new JPanel();
		tabbedPane.addTab("Sand. de vidro", null, abaSandVidro, null);
		abaSandVidro.setLayout(null);

		JLabel lblXTISV = new JLabel("X");
		lblXTISV.setHorizontalAlignment(SwingConstants.CENTER);
		lblXTISV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 16));
		lblXTISV.setBounds(112, 41, 21, 24);
		abaSandVidro.add(lblXTISV);

		JLabel lblXTESV = new JLabel("X");
		lblXTESV.setHorizontalAlignment(SwingConstants.CENTER);
		lblXTESV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 16));
		lblXTESV.setBounds(112, 115, 21, 24);
		abaSandVidro.add(lblXTESV);

		JLabel lblTamanhosInternosSV = new JLabel("Tamanhos internos");
		lblTamanhosInternosSV.setHorizontalAlignment(SwingConstants.CENTER);
		lblTamanhosInternosSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		lblTamanhosInternosSV.setBounds(30, 11, 138, 24);
		abaSandVidro.add(lblTamanhosInternosSV);

		JLabel lblTamanhosInternosExternosSV = new JLabel("Tamanhos externos");
		lblTamanhosInternosExternosSV.setHorizontalAlignment(SwingConstants.CENTER);
		lblTamanhosInternosExternosSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		lblTamanhosInternosExternosSV.setBounds(30, 84, 146, 24);
		abaSandVidro.add(lblTamanhosInternosExternosSV);

		btnReplicarSV.setToolTipText("Copia os valores das caixas de tamanhos internos para externos");
		btnReplicarSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 14));
		btnReplicarSV.setEnabled(false);
		btnReplicarSV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnReplicar(comboBoxTamanhoExterno1SV, comboBoxTamanhoInterno1SV);
				btnReplicar(comboBoxTamanhoExterno2SV, comboBoxTamanhoInterno2SV);
			}
		});
		btnReplicarSV.setBounds(216, 79, 80, 24);
		abaSandVidro.add(btnReplicarSV);

		comboBoxTamanhoInterno1SV.setModel(new DefaultComboBoxModel<>(ComboBoxTamanhos.values()));
		comboBoxTamanhoInterno1SV.setMaximumRowCount(15);
		comboBoxTamanhoInterno1SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		comboBoxTamanhoInterno1SV.setBounds(30, 40, 76, 28);
		abaSandVidro.add(comboBoxTamanhoInterno1SV);

		comboBoxTamanhoInterno2SV.setModel(new DefaultComboBoxModel<>(ComboBoxTamanhos.values()));
		comboBoxTamanhoInterno2SV.setMaximumRowCount(15);
		comboBoxTamanhoInterno2SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		comboBoxTamanhoInterno2SV.setBounds(140, 40, 76, 28);
		abaSandVidro.add(comboBoxTamanhoInterno2SV);

		comboBoxTamanhoExterno1SV.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				comboBoxTamanhosExternosZero(comboBoxTamanhoInterno1SV, comboBoxTamanhoInterno2SV,
						comboBoxTamanhoExterno1SV, comboBoxTamanhoExterno2SV, btnCalcularSV, btnDetalhesSV);
			}
		});
		comboBoxTamanhoExterno1SV.setModel(new DefaultComboBoxModel<>(ComboBoxTamanhos.values()));
		comboBoxTamanhoExterno1SV.setMaximumRowCount(15);
		comboBoxTamanhoExterno1SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		comboBoxTamanhoExterno1SV.setBounds(30, 113, 76, 28);
		abaSandVidro.add(comboBoxTamanhoExterno1SV);

		comboBoxTamanhoExterno2SV.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent e) {
				comboBoxTamanhosExternosZero(comboBoxTamanhoInterno1SV, comboBoxTamanhoInterno2SV,
						comboBoxTamanhoExterno1SV, comboBoxTamanhoExterno2SV, btnCalcularSV, btnDetalhesSV);
			}
		});
		comboBoxTamanhoExterno2SV.setModel(new DefaultComboBoxModel<>(ComboBoxTamanhos.values()));
		comboBoxTamanhoExterno2SV.setMaximumRowCount(15);
		comboBoxTamanhoExterno2SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		comboBoxTamanhoExterno2SV.setBounds(140, 113, 76, 28);
		abaSandVidro.add(comboBoxTamanhoExterno2SV);

		JLabel lblMoldura1SV = new JLabel("Moldura 1 R$");
		lblMoldura1SV.setHorizontalAlignment(SwingConstants.CENTER);
		lblMoldura1SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		lblMoldura1SV.setBounds(400, 43, 101, 22);
		abaSandVidro.add(lblMoldura1SV);

		JLabel lblMoldura2SV = new JLabel("Moldura 2 R$");
		lblMoldura2SV.setHorizontalAlignment(SwingConstants.CENTER);
		lblMoldura2SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		lblMoldura2SV.setBounds(400, 85, 101, 22);
		abaSandVidro.add(lblMoldura2SV);

		JLabel lblMoldura3SV = new JLabel("Moldura 3 R$");
		lblMoldura3SV.setHorizontalAlignment(SwingConstants.CENTER);
		lblMoldura3SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		lblMoldura3SV.setBounds(400, 127, 101, 22);
		abaSandVidro.add(lblMoldura3SV);

		JLabel lblMoldura4SV = new JLabel("Moldura 4 R$");
		lblMoldura4SV.setHorizontalAlignment(SwingConstants.CENTER);
		lblMoldura4SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		lblMoldura4SV.setBounds(400, 169, 101, 22);
		abaSandVidro.add(lblMoldura4SV);

		comboBoxMoldura1SV.setEnabled(false);
		comboBoxMoldura1SV.setModel(new DefaultComboBoxModel<>(ComboBoxMoldura.values()));
		comboBoxMoldura1SV.setMaximumRowCount(15);
		comboBoxMoldura1SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		comboBoxMoldura1SV.setBounds(511, 41, 75, 28);
		abaSandVidro.add(comboBoxMoldura1SV);

		comboBoxMoldura2SV.setEnabled(false);
		comboBoxMoldura2SV.setModel(new DefaultComboBoxModel<>(ComboBoxMoldura.values()));
		comboBoxMoldura2SV.setMaximumRowCount(15);
		comboBoxMoldura2SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		comboBoxMoldura2SV.setBounds(511, 82, 75, 28);
		abaSandVidro.add(comboBoxMoldura2SV);

		comboBoxMoldura3SV.setEnabled(false);
		comboBoxMoldura3SV.setModel(new DefaultComboBoxModel<>(ComboBoxMoldura.values()));
		comboBoxMoldura3SV.setMaximumRowCount(15);
		comboBoxMoldura3SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		comboBoxMoldura3SV.setBounds(511, 124, 75, 28);
		abaSandVidro.add(comboBoxMoldura3SV);

		comboBoxMoldura4SV.setEnabled(false);
		comboBoxMoldura4SV.setModel(new DefaultComboBoxModel<>(ComboBoxMoldura.values()));
		comboBoxMoldura4SV.setMaximumRowCount(15);
		comboBoxMoldura4SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		comboBoxMoldura4SV.setBounds(511, 166, 75, 28);
		abaSandVidro.add(comboBoxMoldura4SV);
		btnCalcularSV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				orcamentoSV.setTamanhoInterno1(getComboBoxTamanho(comboBoxTamanhoInterno1SV));
				orcamentoSV.setTamanhoInterno2(getComboBoxTamanho(comboBoxTamanhoInterno2SV));
				orcamentoSV.setTamanhoExterno1(getComboBoxTamanho(comboBoxTamanhoExterno1SV));
				orcamentoSV.setTamanhoExterno2(getComboBoxTamanho(comboBoxTamanhoExterno2SV));
				orcamentoSV.setMoldura1(getComboBoxMoldura(comboBoxMoldura1SV));
				orcamentoSV.setMoldura2(getComboBoxMoldura(comboBoxMoldura2SV));
				orcamentoSV.setMoldura3(getComboBoxMoldura(comboBoxMoldura3SV));
				orcamentoSV.setMoldura4(getComboBoxMoldura(comboBoxMoldura4SV));
				orcamentoSV.setVidro(getTipoVidro());
				orcamentoSV.setVidroTraseiroSV();
				orcamentoSV.setMaoDeObra(getMaoDeObra(orcamentoSV));
				orcamentoSV.setTotal(somaTotal(orcamentoSV));
				txtValorSV.setText(String.format("%.2f", orcamentoSV.getTotal()));
				btnDetalhesSV.setEnabled(true);
				spinNQuadrosSV.setEnabled(true);
				spinNQuadrosSV.setValue(1);
				menuArquivoSalvar.setEnabled(true);
			}
		});

		btnCalcularSV.setEnabled(false);
		btnCalcularSV.setToolTipText("");
		btnCalcularSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 22));
		btnCalcularSV.setBounds(30, 335, 135, 35);
		abaSandVidro.add(btnCalcularSV);

		JLabel lblTotalSV = new JLabel("Total: R$");
		lblTotalSV.setHorizontalAlignment(SwingConstants.CENTER);
		lblTotalSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 22));
		lblTotalSV.setBounds(395, 339, 97, 28);
		abaSandVidro.add(lblTotalSV);

		btnDetalhesSV.setToolTipText("Exibe os valores de cada material separadamente");
		btnDetalhesSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		btnDetalhesSV.setEnabled(false);
		btnDetalhesSV.setBounds(170, 339, 108, 27);
		abaSandVidro.add(btnDetalhesSV);

		txtValorSV = new JTextField();
		txtValorSV.setText("0,00");
		txtValorSV.setHorizontalAlignment(SwingConstants.RIGHT);
		txtValorSV.setForeground(Color.RED);
		txtValorSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 26));
		txtValorSV.setEditable(false);
		txtValorSV.setColumns(10);
		txtValorSV.setBackground(Color.WHITE);
		txtValorSV.setBounds(493, 335, 135, 35);
		abaSandVidro.add(txtValorSV);

		JLabel lblVidroFrontalSV = new JLabel("Vidro frontal");
		lblVidroFrontalSV.setHorizontalAlignment(SwingConstants.LEFT);
		lblVidroFrontalSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 20));
		lblVidroFrontalSV.setBounds(30, 178, 103, 24);
		abaSandVidro.add(lblVidroFrontalSV);

		rdbtnVidroComumSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		rdbtnVidroComumSV.setBounds(150, 180, 83, 23);
		abaSandVidro.add(rdbtnVidroComumSV);

		rdbtnVidroAntirreflexoSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		rdbtnVidroAntirreflexoSV.setBounds(235, 180, 111, 23);
		abaSandVidro.add(rdbtnVidroAntirreflexoSV);

		buttonGroupSV.add(rdbtnVidroComumSV);
		buttonGroupSV.add(rdbtnVidroAntirreflexoSV);

		chckbxMaoDeObraSV.setSelected(true);
		chckbxMaoDeObraSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		chckbxMaoDeObraSV.setBounds(30, 235, 117, 31);
		abaSandVidro.add(chckbxMaoDeObraSV);

		JLabel txtNQuadrosSV = new JLabel("N. de quadros");
		txtNQuadrosSV.setHorizontalAlignment(SwingConstants.CENTER);
		txtNQuadrosSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		txtNQuadrosSV.setBounds(467, 300, 102, 22);
		abaSandVidro.add(txtNQuadrosSV);

		spinNQuadrosSV.setModel(new SpinnerNumberModel(new Integer(1), new Integer(1), null, new Integer(1)));
		spinNQuadrosSV.setEnabled(false);
		spinNQuadrosSV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.PLAIN, 18));
		spinNQuadrosSV.setBounds(579, 298, 50, 25);
		abaSandVidro.add(spinNQuadrosSV);

		btnAddMoldura2SV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddOnOff(btnAddMoldura1, comboBoxMoldura2SV);
			}
		});
		btnAddMoldura2SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 16));
		btnAddMoldura2SV.setBounds(588, 83, 43, 26);
		abaSandVidro.add(btnAddMoldura2SV);

		btnAddMoldura3SV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddOnOff(btnAddMoldura3SV, comboBoxMoldura3SV);
			}
		});
		btnAddMoldura3SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 16));
		btnAddMoldura3SV.setBounds(588, 125, 43, 26);
		abaSandVidro.add(btnAddMoldura3SV);

		btnAddMoldura4SV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnAddOnOff(btnAddMoldura4SV, comboBoxMoldura4SV);
			}
		});
		btnAddMoldura4SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 16));
		btnAddMoldura4SV.setBounds(588, 167, 43, 26);
		abaSandVidro.add(btnAddMoldura4SV);

		btnAddMoldura1SV.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnAddOnOff(btnAddMoldura1SV, comboBoxMoldura1SV);
			}
		});
		btnAddMoldura1SV.setFont(new Font(TIMES_NEW_ROMAN_FONT, Font.BOLD, 16));
		btnAddMoldura1SV.setBounds(588, 42, 43, 26);
		abaSandVidro.add(btnAddMoldura1SV);

		comboBoxTamanhoInterno1.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				comboBoxTamanhosInternosZero(comboBoxTamanhoInterno1, comboBoxTamanhoInterno2, btnReplicar);
				comboBoxTamanhosExternosZero(comboBoxTamanhoInterno1, comboBoxTamanhoInterno2, comboBoxTamanhoExterno1,
						comboBoxTamanhoExterno2, btnCalcular, btnDetalhes);
			}
		});

		comboBoxTamanhoInterno2.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				comboBoxTamanhosInternosZero(comboBoxTamanhoInterno1, comboBoxTamanhoInterno2, btnReplicar);
				comboBoxTamanhosExternosZero(comboBoxTamanhoInterno1, comboBoxTamanhoInterno2, comboBoxTamanhoExterno1,
						comboBoxTamanhoExterno2, btnCalcular, btnDetalhes);
			}
		});

		comboBoxTamanhoInterno1SV.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				comboBoxTamanhosInternosZero(comboBoxTamanhoInterno1SV, comboBoxTamanhoInterno2SV, btnReplicarSV);
				comboBoxTamanhosExternosZero(comboBoxTamanhoInterno1SV, comboBoxTamanhoInterno2SV,
						comboBoxTamanhoExterno1SV, comboBoxTamanhoExterno2SV, btnCalcularSV, btnDetalhesSV);
			}
		});

		comboBoxTamanhoInterno2SV.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				comboBoxTamanhosInternosZero(comboBoxTamanhoInterno1SV, comboBoxTamanhoInterno2SV, btnReplicarSV);
				comboBoxTamanhosExternosZero(comboBoxTamanhoInterno1SV, comboBoxTamanhoInterno2SV,
						comboBoxTamanhoExterno1SV, comboBoxTamanhoExterno2SV, btnCalcularSV, btnDetalhesSV);
			}
		});
	}

	private void comboBoxTamanhosInternosZero(JComboBox<ComboBoxTamanhos> comboBoxInterno1,
			JComboBox<ComboBoxTamanhos> comboBoxInterno2, JButton btnReplicar) {
		if (comboBoxInterno1.getSelectedIndex() == 0 || comboBoxInterno2.getSelectedIndex() == 0) {
			btnReplicar.setEnabled(false);
		} else {
			btnReplicar.setEnabled(true);
		}
	}

	private void comboBoxTamanhosExternosZero(JComboBox<ComboBoxTamanhos> cBTamanhoInterno1,
			JComboBox<ComboBoxTamanhos> cBTamanhoInterno2, JComboBox<ComboBoxTamanhos> cBTamanhoExterno1,
			JComboBox<ComboBoxTamanhos> cBTamanhoExterno2, JButton btnCalc, JButton btnDetalhes) {
		if (cBTamanhoInterno1.getSelectedIndex() == 0 || cBTamanhoInterno2.getSelectedIndex() == 0
				|| cBTamanhoExterno1.getSelectedIndex() == 0 || cBTamanhoExterno2.getSelectedIndex() == 0) {
			btnCalc.setEnabled(false);
			btnDetalhes.setEnabled(false);
		} else {
			btnCalc.setEnabled(true);
		}
	}

	private void btnAddOnOff(JButton btnAddMoldura, JComboBox<ComboBoxMoldura> comboBoxMoldura) {
		if (btnAddMoldura.getText() == "+") {
			btnAddMoldura.setText("-");
			comboBoxMoldura.setEnabled(true);
		} else if (btnAddMoldura.getText() == "-") {
			comboBoxMoldura.setEnabled(false);
			comboBoxMoldura.setSelectedIndex(0);
			btnAddMoldura.setText("+");
		}
	}

	private void btnReplicar(JComboBox<ComboBoxTamanhos> comboBoxExterno, JComboBox<ComboBoxTamanhos> comboBoxInterno) {
		comboBoxExterno.setSelectedIndex(comboBoxInterno.getSelectedIndex());
	}

	private double getComboBoxTamanho(JComboBox<ComboBoxTamanhos> comboBoxTamanho) {
		int length = comboBoxTamanho.getSelectedItem().toString().replace(" cm", "").replace(" m", "").length();
		if (length <= 2) {
			return Double.parseDouble("0.".concat(comboBoxTamanho.getSelectedItem().toString().replace(" cm", "")));
		} else {
			return Double.parseDouble(comboBoxTamanho.getSelectedItem().toString().replace(" cm", "").replace(" m", "")
					.replace(",", "."));
		}
	}

	private int getComboBoxMoldura(JComboBox<ComboBoxMoldura> comboBoxMoldura) {
		return Integer.parseInt(comboBoxMoldura.getSelectedItem().toString().replace(",00", ""));
	}

	private final int getTipoVidro() {
		if (rdbtnVidroComum.isSelected()) {
			return Orcamento.VIDRO_COMUM;
		} else if (rdbtnVidroAntirreflexo.isSelected()) {
			return Orcamento.VIDRO_ANTIRREFLEXO;
		} else {
			return Orcamento.SEM_VIDRO;
		}
	}

	private double getMaoDeObra(Orcamento orcamento) {
		if (chckbxMaoDeObra.isSelected()) {
			return orcamento.getTamanhoExterno() * Orcamento.MAO_DE_OBRA;
		} else {
			return 0;
		}
	}

	private double getEucatex(Orcamento orcamento) {
		if (chckbxEucatex.isSelected()) {
			return (orcamento.getTamanhoInterno1() * orcamento.getTamanhoInterno2()) * Orcamento.EUCATEX_VALOR;
		} else {
			return 0;
		}
	}

	private double getPaspatur(Orcamento orcamento) {
		if (chckbxPaspatur.isSelected()) {
			return orcamento.getAreaInterno() * Orcamento.PASPATUR_VALOR;
		} else {
			return 0;
		}
	}

	private int getSarrafo() {
		if (chckbxSarrafo.isSelected()) {
			return Orcamento.SARRAFO_VALOR;
		} else {
			return 0;
		}
	}

	private double getEspelho(Orcamento orcamento) {
		if (chckbxEspelho.isSelected()) {
			return orcamento.getAreaInterno() * Orcamento.ESPELHO_VALOR;
		} else {
			return 0;
		}
	}

	private double somaTotal(Orcamento orcamento) {
		return orcamento.getMoldura1() + orcamento.getMoldura2() + orcamento.getMoldura3() + orcamento.getMoldura4()
				+ orcamento.getVidro() + orcamento.getMaoDeObra() + orcamento.getEucatex() + orcamento.getPaspatur()
				+ orcamento.getSarrafo() + orcamento.getEspelho();
	}

	private void precoMinimo(double total) {
		if (total <= 15) {
			lblPrecoMinimo.setVisible(true);
		} else {
			lblPrecoMinimo.setVisible(false);
		}
	}
}
