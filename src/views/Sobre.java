package views;

import java.net.MalformedURLException;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import java.awt.Toolkit;
import javax.swing.SwingConstants;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Sobre extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3018876497052995069L;
	private JPanel contentPane;
	private JLabel magicGif;
	private JLabel lblProduto;
	private JLabel lblAutorLucasDavi;
	private JLabel label;
	private JButton lblOk;

	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 * 
	 * @throws MalformedURLException
	 */
	public Sobre() {
		setResizable(false);
		setIconImage(Toolkit.getDefaultToolkit().getImage(Sobre.class.getResource("/images/icon.png")));
		setTitle("Sobre");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 540, 248);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		setLocationRelativeTo(null);

		magicGif = new JLabel("");
		magicGif.setIcon(new ImageIcon(Sobre.class.getResource("/images/magic.gif")));
		magicGif.setBounds(0, 0, 240, 219);
		contentPane.add(magicGif);

		lblProduto = new JLabel("Or\u00E7amento - S\u00F3 Posters v1.0");
		lblProduto.setFont(new Font("Segoe Print", Font.PLAIN, 16));
		lblProduto.setHorizontalAlignment(SwingConstants.RIGHT);
		lblProduto.setBounds(270, 23, 238, 30);
		contentPane.add(lblProduto);

		lblAutorLucasDavi = new JLabel("Lucas Davi Medeiros");
		lblAutorLucasDavi.setHorizontalAlignment(SwingConstants.RIGHT);
		lblAutorLucasDavi.setFont(new Font("Segoe Print", Font.PLAIN, 16));
		lblAutorLucasDavi.setBounds(270, 64, 238, 30);
		contentPane.add(lblAutorLucasDavi);

		label = new JLabel("07/2018");
		label.setHorizontalAlignment(SwingConstants.RIGHT);
		label.setFont(new Font("Segoe Print", Font.PLAIN, 16));
		label.setBounds(270, 105, 238, 30);
		contentPane.add(label);

		lblOk = new JButton("OK");
		lblOk.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				dispose();
			}
		});
		lblOk.setFont(new Font("Tahoma", Font.BOLD, 14));
		lblOk.setBounds(333, 165, 111, 30);
		contentPane.add(lblOk);

	}
}
